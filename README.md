# Matériel non utilisé

Pour modifier ce document, faire une merge-request ou contacter cle-paris@inria.fr.

## Écrans

| inventaire | modèle       | taille | format | résolution | connectique    | emplacement | contact                   | état | date d’ajout |
|------------|--------------|--------|--------|------------|----------------|-------------|---------------------------|------|--------------|
| 02003032   | Dell U2412M  | 24"    | 16:10  | 1920x1200  | DVI-D, DP, VGA | A121        | thierry.martinez@inria.fr | bon  | 2023/06/01   |
| 02007731   | Dell U2422HE | 23,8"  | 16:9   | 1920x1080  | HDMI, DP       | A121        | thierry.martinez@inria.fr | bon  | 2023/08/09   |
